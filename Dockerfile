FROM crystallang/crystal:latest

WORKDIR /app

COPY shard.yml shard.lock ./

RUN shards install --error-trace

COPY . /app

RUN set -ex && \
    crystal bin/ameba.cr && \
    crystal spec --error-trace && \
    :
